### Forever loop script

```powershell
while(1) { sleep -sec 15; . D:\path\toFile\script.ps1 }
```

### Add cookie to session

```powershell
$cookie = New-Object System.Net.Cookie 
$cookie.Name = "cookieName"
$cookie.Value = "valueOfCookie"
$cookie.Domain = "domain.for.cookie.com"
$session.Cookies.Add($cookie);
```

### RegExp search in string
```powershell
$str = "to jest ciąg znaków z cyframi 2468 do testu regexp"
$reg = $str -match '([0-9]+)'
$digits= $Matches.1
write-host $digits
```

### Take Screenshot and save to file
```powershell
[Reflection.Assembly]::LoadWithPartialName("System.Drawing")
function screenshot([Drawing.Rectangle]$bounds, $path) {
   $bmp = New-Object Drawing.Bitmap $bounds.width, $bounds.height
   $graphics = [Drawing.Graphics]::FromImage($bmp)

   $graphics.CopyFromScreen($bounds.Location, [Drawing.Point]::Empty, $bounds.size)

   $bmp.Save($path)

   $graphics.Dispose()
   $bmp.Dispose()
}

$bounds = [Drawing.Rectangle]::FromLTRB(0, 0, 1024, 800)
screenshot $bounds "C:\screenshot.png"
```