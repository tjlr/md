this._Thunder = function () {
    this.ctx.beginPath();
    this.ctx.moveTo(this.x2 + this.x8, this.x2);
    this.ctx.lineTo(this.x2 - this.x8, this.x2 + this.x4);
    this.ctx.lineTo(this.x2 - this.x32, this.x2 + this.x4);
    this.ctx.lineTo(this.x2 - this.x8, this.x);
    this.ctx.lineTo(this.x2 + this.x8, this.x2 + this.x8 + this.x16);
    this.ctx.lineTo(this.x2 + this.x32, this.x2 + this.x8 + this.x16);
    this.ctx.closePath();
    this.ctx.fillStyle = this.lineColor;
    this.ctx.fill();
}