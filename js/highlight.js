function hi(el) { highlight(el,255,255,102,255,255,255); }
function highlight(el,r,g,b,r2,g2,b2) {
    _q(el).style.backgroundColor = "rgb("+r+","+g+","+b+")";
    if (r == r2 && g == g2 && b == b2) { return; }
    var newr = r + Math.ceil((r2 - r)/10);
    var newg = g + Math.ceil((g2 - g)/10);
    var newb = b + Math.ceil((b2 - b)/10);
    setTimeout(function() { highlight(el,newr,newg,newb,r2,g2,b2) },25);
}

// example
// hi(document.querySelector('cssSelector'));